var webpack = require('webpack');

var configuration =
    {
        entry:
        {
            page1: './page1.js',
            page2: './page2.js',
        },
        output:
        {
            filename: '[name]-bundle.js',
        },
        plugins: [
            new webpack.optimize.CommonsChunkPlugin('common-bundle.js'),
        ]
    };

module.exports = configuration;