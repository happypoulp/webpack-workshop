var webpack = require('webpack'),
    path = require('path'),
    ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');

var configuration =
    {
        entry:
        {
            page1: './js/page1.js',
            page2: './js/page2.js',
            page3: './js/page3.js',
        },
        output:
        {
            path: 'build',
            publicPath: 'build/',
            filename: '[name].[chunkhash].js',
            chunkFilename: '[id].[chunkhash].js'
        },
        plugins: [
            new webpack.NewWatchingPlugin(),
            new webpack.optimize.CommonsChunkPlugin('common.[chunkhash].js'),
            new ChunkManifestPlugin(
            {
                filename: "webpack_manifest.json",
                manifestVariable: "__webpack_manifest__"
            }),
            function()
            {
                this.plugin("done", function(stats)
                {
                    require("fs").writeFileSync(
                        path.join(__dirname, 'webpack_files_map.conf.json'),
                        JSON.stringify(stats.toJson().assetsByChunkName)
                    );
                });
            }
        ],
        recordsPath: path.join(__dirname, "webpack.records.json"),
        resolve:
        {
            root: path.join(__dirname, 'js')
        }
    };

module.exports = configuration;