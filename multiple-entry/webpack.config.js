var configuration =
    {
        entry:
        {
            page1: './page1.js',
            page2: './page2.js',
        },
        output:
        {
            filename: '[name]-bundle.js',
        }
    };

module.exports = configuration;