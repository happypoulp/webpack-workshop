// Module A
var moduleB = require('./moduleB');
var moduleC = require('./moduleC');

module.exports = function()
{
    console.log('ModuleA');
    moduleB();
    moduleC();
};