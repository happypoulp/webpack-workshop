var configuration =
    {
        entry:
        {
            pack: './page.js',
        },
        output:
        {
            filename: '[name]-bundle.js',
        }
    };

module.exports = configuration;